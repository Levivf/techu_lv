console.log("Aqui funcionando con nodemon");

var movimientosJSON = require('./movimientos2.json')
var usuariosJSON = require('./usuarios.json')
var express = require('express')
var bodyparser = require('body-parser')
var jsonQuery = require('json-query')
fs = require('fs')
var app = express()
var requestJson = require('request-json')
//var libropdf = require('./ElPrincipito.pdf')
app.use(bodyparser.json())

app.get('/',function(req,res)
{
  res.send('hola API')
} )

app.get('/v1/movimientos',function(req,res)
{
  res.sendfile("movimientosv1.json")
} )

app.get('/v4', function (req, res) {
    var filePath = "./ElPrincipito.pdf";
    fs.readFile(filePath , function (err,data){
      //console.log(data)
        res.contentType("application/pdf");
        res.send(data);
    });
});

app.get('/v2/movimientos',function(req,res)
{
  res.send(movimientosJSON)
} )

app.get('/v2/movimientos/:id', function(req,res)
{
  console.log(req.params.id)
  //console.log(movimientosJSON)
  //res.send("Hemos recibido su peticion del movimiento#" + req.params.id)
  res.send(movimientosJSON[req.params.id-1])
}
)

app.get('/v3/movimientos',function(req,res)
{
  res.send(movimientosJSON)
} )

app.get('/v3/movimientos/:id', function(req,res)
{
  console.log(req.params.id)
  //console.log(movimientosJSON)
  //res.send("Hemos recibido su peticion del movimiento#" + req.params.id)
  res.send(movimientosJSON[req.params.id-1])
}
)

app.get('/v2/movimientosq', function(req,res)
{
  console.log(req.query)
  res.send("recibido")
}
)

app.get('/v2/movimientosp/:id', function(req,res)
{
  console.log(req.params)
  res.send("recibido")
}
)

app.post('/v2/movimientos', function(req,res)
{/*console.log(req)
  if (req.headers['authorization'] == undefined){
  res.send("No autorizado")
}
else {*/
  var nuevo = req.body
  nuevo.id = movimientosJSON.length + 1
  movimientosJSON.push(nuevo)      //agrega un movimiento
  res.send("movimiento dado de alta")
//}
}
)

app.put('/v2/movimientos/:id', function (req, res)
{
  var actual = movimientosJSON[req.params.id-1];
  var cambios = req.body;
if(cambios.importe !=undefined)
{
  actual.importe = cambios.importe
}

if(cambios.ciudad !=undefined)
{
  actual.ciudad = cambios.ciudad
}

/*  var nuevo = req.body;
  movimientosJSON[req.params.id-1]= nuevo;*/

  res.send("Movimiento actualizado");
});

app.delete('/v2/movimientos/:id', function(req,res)
{//console.log(req)
  var actual = movimientosJSON[req.params.id-1];
  movimientosJSON.push({
    "id":movimientosJSON.length +1,
    "ciudad":actual.ciudad,
    "importe":actual.importe * (-1),
    "concepto": "Negativo del " + req.params.id
  })
  res.send("movimiento anuladodo")
}
)

app.get('/v1/usuario/', function(req,res)
{
  //console.log(usuariosJSON)
  res.send(usuariosJSON)
}
)

app.get('/v1/usuario/:id', function(req,res)
{
  //console.log(req.params.id)
  res.send(usuariosJSON[req.params.id-1])
}
)

app.post('/v1/logout/:id', function(req,res)
{
  var viejo = req.body
  var nuevo = usuariosJSON[req.params.id-1];
  nuevo.sesion = false
  usuariosJSON.push(nuevo)      //actualiza sesion como false
  res.send('{"status":{"code": 200, "description": "Logout Exitoso"}')
} )


//HACER LOGIN DE USUARIO
app.post('/v2/login/:id', function(req,res)
{
  var viejo = req.body;
var nuevo = usuariosJSON[req.params.id-1];
if (viejo.email == nuevo.email && viejo.password==nuevo.password)
  {
    res.send('{"status":{"code": 200, "description": "Exito"}')
  }
  else {
    res.send('{"description": "Usuario no autenticado","status": 401,"errorCode": "NA001"}')
  }
})



//version 3 de API conectada a MLab
var urlMLabRaiz = "https://api.mlab.com/api/1/databases/techumxlv/collections"
var apiKey = "apiKey=7uROiWffmT-bYpjAYlGVb17kmRaGh3pe"
var clienteMlab = requestJson.createClient(urlMLabRaiz + "?" + apiKey)
app.get('/v3', function(req,res)
{
  clienteMlab.get('', function(err, resM, body) {
    var coleccionesUsuario = []
    if (!err)
    {
      for (var i = 0; i < body.length; i++) {
        if(body[i] != "system.indexes"){
          //coleccionesUsuario.push(body[i])
          coleccionesUsuario.push({"recurso":body[i], "url":"/v3/" + body[i]})
        }
      }
      res.send(coleccionesUsuario)
    }
  else {res.send(err)}
  })
})

app.get('/v3/cuentas/:id', function(req,res)
{
clienteMlab = requestJson.createClient(urlMLabRaiz + "/cuentas")
clienteMlab.get('?q={"idCuenta":"' + req.params.id + '"}&f{"movimientos":1}&' + apiKey, function(err, resM, body) {
  res.send(body)
  console.log(req.params.id)
})
})
app.get('/v4/cuentas/:id', function(req,res)
{
clienteMlab = requestJson.createClient(urlMLabRaiz + "/cuentas")
clienteMlab.get('?q={"usuario":"' + req.params.id + '"}&' + apiKey, function(err, resM, body) {
  res.send(body)
  console.log(req.params.id)
})
})
app.get('/v3/cuentas', function(req,res)
{
clienteMlab = requestJson.createClient(urlMLabRaiz + "/cuentas?" + apiKey)
clienteMlab.get('', function(err, resM, body) {
  res.send(body)
})
})

//
app.get('/v3/usuarios/:id', function(req,res)
{
clienteMlab = requestJson.createClient(urlMLabRaiz + "/usuarios")
clienteMlab.get('?q={"idusuario":"' + req.params.id + '"}&' + apiKey, function(err, resM, body) {
  res.send(body)
})
})

app.get('/v3/usuarios', function(req,res)
{
clienteMlab = requestJson.createClient(urlMLabRaiz + "/usuarios?" + apiKey)
clienteMlab.get('', function(err, resM, body) {
  res.send(body)
})
})

app.put('/v3/usuarios/:id', function(req,res)
{
clienteMlab = requestJson.createClient(urlMLabRaiz + "/usuarios")
var cambio = '{"$set":'+ JSON.stringify(req.body) + '}'
console.log(req.body)
clienteMlab.put('?q={"id":' + req.params.id + '}&' + apiKey, JSON.parse(cambio), function(err, resM, body) {
  res.send(body)
})
})

app.post('/v3/usuarios', function(req,res)
{
clienteMlab = requestJson.createClient(urlMLabRaiz + "/usuarios?" + apiKey)
clienteMlab.get('', function(err, resM, body) {
var nuevo = req.headers;
var encontrado = false;
  for (var i = 0; i < body.length; i++) {
    if(body[i].email == nuevo.email && body[i].password==nuevo.password){
      nuevo.idusuario=body[i].idusuario;
      console.log(nuevo.idusuario);
       encontrado = true;
    }
  }
if(encontrado ==true)
{
    res.send('{"status":{"code": 200, "description": "Exito"}},{"usuario":"'+nuevo.idusuario+'"}')
  }
  else {
      res.send('{"status":{"code": 401, "description": "No Autorizado"}}')
    }
})
})


app.post('/v4/usuarios', function(req,res)
{
clienteMlab = requestJson.createClient(urlMLabRaiz + "/usuarios?" + apiKey)
clienteMlab.get('', function(err, resM, body) {
var nuevo = req.body;
console.log(req.body);
var encontrado = false;
  for (var i = 0; i < body.length; i++) {
    if(body[i].email == nuevo.email && body[i].password==nuevo.password){
       encontrado = true;
    }
  }
if(encontrado ==true)
{
    res.send('{"status":{"code": 200, "description": "Exito"}}')
  }
  else {
      res.send('{"status":{"code": 401, "description": "No Autorizado"}}')
    }
})
})


app.listen(3000)
console.log("escuchando en el puerto 3000")
