var mocha = require('mocha')
var chai = require('chai')
var chaiHttp = require('chai-http')
var should = chai.should()
var server = require('../server')

chai.use(chaiHttp)      //Config chai con el módulo http

describe('Tests de conectividad', () => {
  it ('Google funciona', (done) =>{
    chai.request('http://www.google.com.mx')
    .get('/')
    .end((err, res) => {
      //console.log(res)
      res.should.have.status(200)  //should valida el res y ve si su estatus es 200
      done()
    })
  })
})

describe('Tests de API Usuario', () => {
  it ('Raiz de la API funciona', (done) =>{
    chai.request('http://localhost:3000')
    .get('/v3')
    .end((err, res) => {
      //console.log(res)
    console.log(res.body)
      res.should.have.status(200)  //should valida el res y ve si su estatus es 200
      res.body.should.be.a('array')
      res.body.length.should.be.eql(2)
      done()
    })
  })
//
  it ('Raiz de la API devuelve objetos correctos', (done) =>{
    chai.request('http://localhost:3000')
    .get('/v3')
    .end((err, res) => {
      //console.log(res)

    console.log(res.body)
      res.should.have.status(200)  //should valida el res y ve si su estatus es 200
      res.body.should.be.a('array')
      res.body.length.should.be.eql(2)
      for (var i = 0; i < res.body.length; i++) {
        res.body[i].should.have.property('recurso')
        res.body[i].should.have.property('url')
      }
      done()
    })
  })
var movs = 0
    it ('Raiz de la API movimientos contesta', (done) =>{
      chai.request('http://localhost:3000')
      .get('/v3/usuarios')
      .end((err, res) => {
        console.log(res.body)
        res.should.have.status(200)
          res.body.should.be.a('array')
          var movs =res.body.length +1
          console.log(movs)
            //should valida el res y ve si su estatus es 200
        done()
      })
    })

    it ('Raiz de la API ID de movimientos específico', (done) =>{
      chai.request('http://localhost:3000')
     .get('/v3/usuarios/4')
      .end((err, res) => {
        console.log(res.body)
        res.should.have.status(200)
        //res.body.should.have.property('id')
         res.body.should.be.a('array')
          res.body[0].id.should.equals(4)
          console.log(movs)
          //for (var i = 0; i < 56; i++) {
          // res.body[i].should.have.property('id')
        // }
            //should valida el res y ve si su estatus es 200
        done()
      })
    })

})
